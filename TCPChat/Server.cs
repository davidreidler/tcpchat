﻿using System;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCPChat
{
	public delegate void ErrorEventHandler(Server sender, ServerEventArgs e);
	public delegate void UpdateEventHandler(Server sender, ServerEventArgs e);

	public class Server
	{
		private bool shuttingDown = false;
		private IPEndPoint endPoint;
		private TcpListener listener;

		public event ErrorEventHandler OnError;
		public event UpdateEventHandler OnUpdate;
		/// <summary>
		/// Initialize the data in the server and load it into memory.
		/// </summary>
		/// <param name="ip">The IP address string that the server will listen on.</param>
		/// <param name="port">The port that the server will listen on.</param>
		public Server(string ip, int port)
		{
			try
			{
				// Create the end point and socket
				endPoint = new IPEndPoint(IPAddress.Parse(ip), port);
				listener = new TcpListener(endPoint);
			}
			catch (Exception e)
			{
				Error(e.Message);
			}
		}

		/// <summary>
		/// Starts the server and puts it into an active listening state.
		/// </summary>
		public void Start()
		{
			// Networking wasn't configured correctly, thus we can't start
			if (endPoint == null || listener == null)
			{
				Error("Couldn't start the server! A setting might have been misconfigured.");
				return;
			}

			// We do not want to start if we are already shutting down
			if (shuttingDown)
			{
				Error("We cannot start while shutting down!");
				return;
			}

			// We're going to use asynchronous network IO.
			try
			{
				listener.Start();
				Update("Server has started!");
				Update("Waiting for a connection...");
				listener.BeginAcceptTcpClient(AcceptCallback, listener);
				shuttingDown = false;
			}
			catch (Exception e)
			{
				Error("Something happened while starting the server!");
				Error(e.StackTrace + e.Message);
			}
		}

		/// <summary>
		/// Shuts down the server, stops listening for connections.
		/// </summary>
		public void Stop()
		{
			// Don't try to shut down again!
			if (shuttingDown) return;

			shuttingDown = true;
			listener.Stop();
			Update("Server has shutdown.");
		}

		/// <summary>
		/// Called when the listener attempts to establish a new connection.
		/// Puts the listener back into a listen state to accept more clients.
		/// </summary>
		/// <param name="result">The TcpListener object passed by TcpListener.BeginAcceptTcpClient().</param>
		private void AcceptCallback(IAsyncResult result)
		{
			// TODO: We might want to do some kind of notification to the user
			if (shuttingDown) return;
			try
			{
				// Establish the connection and listener again
				TcpListener listener = (TcpListener)result.AsyncState;
				TcpClient client = listener.EndAcceptTcpClient(result);

				BufferStateObject buf = new BufferStateObject(client);

				// Continue listening
				listener.BeginAcceptTcpClient(AcceptCallback, listener);

				// Start reading the socket stream
				buf.BeginRead(ReadCallback);

				Update("Connected to the server!");
			}
			catch (Exception e)
			{
				Error(e.StackTrace + e.Message + Environment.NewLine + e.TargetSite + e.StackTrace);
			}
		}
			
		/// <summary>
		/// Called after the remote host has sent some data.
		/// </summary>
		/// <param name="result"></param>
		// TODO: Stronger error detetection for disconnects.
		private void ReadCallback(IAsyncResult result)
		{
			try
			{
				// Get our Buffer object casted correctly
				BufferStateObject buffer = (BufferStateObject)result.AsyncState;
				int rec = buffer.EndRead(result);

				if(rec == 0)
				{
					Update("No data was read. (Remote host probably disconnected)");
					return;
				}

				// Still need to read data
				if(!buffer.IsFull)
				{
					// We need the header first
					if(!buffer.Reading)
					{
						if(rec != 5)
						{
							Error("Failed to get the full header! (Remote host may have disconnected)");
							buffer.Close();
							return;
						}

						// Start reading the header
						buffer.BeginRead(ReadCallback);
					}
					// We need to start reading the data buffer
					else 
					{
						buffer.BeginRead(ReadCallback);
					}
				}
				// We have finished reading
				else
				{
					// Read Message and continue to read from the client
					Update(buffer.RemoteEndPoint.ToString() + " wrote: " + buffer.ToString());
					buffer.Clear();
					buffer.BeginRead(ReadCallback);
				}
			}
			catch (Exception e)
			{
				Error(e.Message + e.StackTrace);
			}
		}

		protected virtual void Error(string message)
		{
			if (OnError != null)
				OnError(this, new ServerEventArgs("[ERROR]: " + message));
		}

		protected virtual void Update(string message)
		{
			if (OnUpdate != null)
				OnUpdate(this, new ServerEventArgs("[UPDATE]: " + message));
		}
	}

	public class ServerEventArgs : EventArgs
	{
		public readonly string message;

		public ServerEventArgs(string message)
		{
			this.message = message;
		}
	}
}
﻿using System;

namespace TCPChat
{
	public struct MessageHeader 
	{
		public byte msgType;
		public int msgSize;

		public MessageHeader(byte type, int size)
		{
			msgType = type;
			msgSize = size;
		}
	}
}
﻿using System;
using System.IO;
using System.Net;
using System.Net.Sockets;
using System.Runtime.Serialization;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;

namespace TCPChat
{
	class Program
	{
		private static Server server;

		public static void Main(string[] args)
		{
			try
			{
				server = new Server("127.0.0.1", 11000);

				server.OnError += Server_OnError;
				server.OnUpdate += Server_OnUpdate;

				server.Start();

				StartRougeConnection();

				Console.ReadLine();

				Console.WriteLine("Finished now. Shutting down...");
				server.Stop();
			} 
			catch (Exception e) 
			{
				Console.WriteLine(e.Message);
			}

			Console.WriteLine("Server has shutdown. Exiting Program...");
			Console.ReadLine();
		}

		static void Server_OnUpdate(Server sender, ServerEventArgs e)
		{
			Console.WriteLine(e.message);
		}

		static void Server_OnError(Server sender, ServerEventArgs e)
		{
			Console.WriteLine(e.message);
		}

		static void StartRougeConnection()
		{
			try {
				TcpClient client = new TcpClient();
				client.Connect(new IPEndPoint(IPAddress.Parse ("127.0.0.1"), 11000));

				string msg = "HI THERE!";
				byte[] header = { 5, 0, 0, 0, 0 };
				Array.Copy(BitConverter.GetBytes(msg.Length), 0, header, 1, 4);

				Console.WriteLine("Starting Rouge Connection...");

				client.GetStream().Write(header, 0, 5);
				client.GetStream().Write(Encoding.ASCII.GetBytes(msg), 0, msg.Length);

				Console.WriteLine("Writing second message");

				msg = "I'M DOING WELL, I HOPE YOU ARE AS WELL!";
				Array.Copy(BitConverter.GetBytes(msg.Length), 0, header, 1, 4);

				client.GetStream().Write(header, 0, 5);
				client.GetStream().Write(Encoding.ASCII.GetBytes(msg), 0, msg.Length);

				Console.WriteLine("Done with Rouge connection.");
				client.Close();

			} catch (Exception e) {
				Console.WriteLine ("[ROUGE]: Something went wrong - " + e.Message);
			}
		}
	}
}
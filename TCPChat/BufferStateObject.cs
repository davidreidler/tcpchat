﻿using System;
using System.Collections.Generic;
using System.Net;
using System.Net.Sockets;
using System.Text;

namespace TCPChat
{
	public class BufferStateObject
	{
		private int recieved;
		private bool reading = false;
		private byte[] msgHeader = new byte[5];
		private byte[] dataBuffer;

		private TcpClient client;

		//public byte[] DataBuffer { get { return this.dataBuffer; } }
		public bool Reading { get { return this.reading; } }
		public bool IsFull { get { return this.recieved == Header.msgSize; } }
		public MessageHeader Header
		{
			get { return new MessageHeader(msgHeader[0], BitConverter.ToInt32(msgHeader, 1)); }
		}
		public EndPoint RemoteEndPoint { get { return client.Client.RemoteEndPoint; } }

		public BufferStateObject(TcpClient client)
		{
			this.client = client;
			this.dataBuffer = null;
			this.recieved = -1;
		}

		public void BeginRead(AsyncCallback callback)
		{
			// We need a header
			if(this.recieved == -1)
			{
				this.reading = true;
				this.client.GetStream().BeginRead(msgHeader, 0, 5, callback, this);
			} 
			// We more data
			else if(!IsFull)
			{
				//Console.WriteLine("Writing {0} bytes to data buffer.", Header.msgSize - recieved);
				this.client.GetStream().BeginRead(dataBuffer, recieved, Header.msgSize - recieved, callback, this);
			} 
		}

		public int EndRead(IAsyncResult res)
		{
			int ret = client.GetStream().EndRead(res);

			// TODO: Some corner cases for no data transmission

			if(ret == 0) 
			{
				return ret;
			}

			// We just finished reading the header - now get ready for the data
			if(this.recieved == -1) 
			{
				this.recieved = 0;
				this.reading = true;
				this.dataBuffer = new byte[Header.msgSize];
			} 
			// Figure out if we need more data
			else 
			{
				this.recieved += ret;
				this.reading = !this.IsFull;
			}

			return ret;
		}

		public void Close()
		{
			this.Clear();
			this.client.Close();
		}
	
		public override string ToString()
		{
			return Encoding.ASCII.GetString(this.dataBuffer, 0, this.recieved);
		}

		public void Clear()
		{
			// We have all the data - write it out and get rid of the buffer
			this.recieved = -1;
			this.reading = false;
			this.dataBuffer = null;
			Array.Clear(this.msgHeader, 0, 5);
		}
	}
}
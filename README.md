# TCP Chat
This is a small Client/Server package that aims to be a lightweight server for simple text based communications. It aims to support the following features:

	- Group chat
	- Configurable Server
	- Basic utility commands
	- Authentication
	- Encrypted connections
	- Extensible 
	
This is largely a work in progress, and is still very early in development. Right now, only basic network communications are supported (ie creating listening socket, and reading data). This is also a rewrite of the entire project, so there is very little done yet. I used to have a working interface, but it slowly became unmanageable and cluttered due to my inexperience.